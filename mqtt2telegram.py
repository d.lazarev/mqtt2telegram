#!/usr/bin/env python3
from configparser import ConfigParser
import paho.mqtt.client as mqtt
import requests

config = ConfigParser()
config.read('/etc/mqtt2telegram.ini')
username = config.get('auth', 'username')
passwd = config.get('auth', 'password')
url = 'https://api.telegram.org/bot897192329:AAH4w-AK-jiOtrd-3thLZHU5ie6F9iKVAEg/sendMessage'
chat_id='153958539'

def on_message(client, userdata, message):
    msg = str(message.payload.decode("utf-8"))
    topic = message.topic
    print("message received: ", msg)
    print("message topic=", topic)
    post_data = {'chat_id': chat_id, 'text': msg}
    x = requests.post(url, data = post_data)
    print(x)

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.subscribe("LDmsgSender/#")
    else:
        print("Connection return result: ", rc)
        exit(-1)

broker_addr = "perets.su"
client = mqtt.Client("LDmsgSender", clean_session=True)
client.username_pw_set(username=username, password=passwd)
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker_addr, 1883, 60)

client.publish("LDmsgSender/s1", "I'm python daemon")
client.loop_forever()
